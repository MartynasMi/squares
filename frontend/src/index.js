import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import PointList from "./PointList";

var destination = document.querySelector("#container");

ReactDOM.render(
    <div>
        <PointList/>
    </div>,
    destination
);
