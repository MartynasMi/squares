import React, { Component } from "react";

class PointItems extends Component {

  constructor(props) {
    super(props);

    this.createPoints = this.createPoints.bind(this);
  }

  delete(key) {
    this.props.delete(key);
  }

  createPoints(point) {
    return <li key={point.key}>
              {point.x};{point.y}
              <button onClick={() => this.delete(point.key)}>Delete</button>
           </li>
  }

  render() {
    var pointEntries = this.props.points;
    var pointItems = pointEntries.map(this.createPoints);

    return (
      <ul className="pointList">
          {pointItems}
      </ul>
    );
  }

}

export default PointItems;
