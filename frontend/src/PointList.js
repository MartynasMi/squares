import React, { Component } from "react";
import PointItems from "./PointItems";
import "./PointList.css";
import FileSaver from 'file-saver';
import Modal from 'react-modal';

class PointList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      points: [],
      modalIsOpen: false,
      errorMessage: null
    };

    this.addPoint = this.addPoint.bind(this);
    this.deletePoint = this.deletePoint.bind(this);

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  closeModal() {
    this.setState({
      modalIsOpen: false,
      errorMessage: null
    });
  }

  addPoint(e) {
    if (this.state.points.length >= 1000) {
      this.setState({
        errorMessage: "List is full"
      });
      this.openModal();
    } else

    if (this._inputElementX.value == "" || this._inputElementY.value == "") {
      this.setState({
        errorMessage: "Input field is empty"
      });
      this.openModal();
    } else {

        if (!this.intVal(this._inputElementX.value) || !this.intVal(this._inputElementY.value)) {
          this.setState({
            errorMessage: "Coordinates should be an integer"
          });
          this.openModal();
        } else {

          var xNumber = parseInt(this._inputElementX.value, 10)
          var yNumber = parseInt(this._inputElementY.value, 10)

          if (xNumber >= -5000 && xNumber <= 5000 && yNumber >= -5000 && yNumber <= 5000) {

            var newPoint = {
              x: xNumber,
              y: yNumber,
              key: Date.now()
            };

            this.setState((prevState) => {
              return {
                points: prevState.points.concat(newPoint)
              };
            });

            this._inputElementX.value = "";
            this._inputElementY.value = "";

          } else {
            this.setState({
              errorMessage: "Coordinates should be in range [-5000; 5000]"
            });
            this.openModal();
          }
        }
    }

    console.log(this.state.points);

    e.preventDefault();
  }

  deletePoint(key) {
    var filteredPoints = this.state.points.filter(function (point) {
      return (point.key !== key);
    });

    this.setState({
      points: filteredPoints
    });
  }

  deleteAll() {
    console.log("all");
    this.setState({
      points: []
    });
  }

  retrieveFromServer() {
    fetch('/rest/points/getAllPoints', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Accept-Language': 'en-US,en;q=0.9',
        'Content-Type': 'application/json',
        mode: 'no-cors'
      }
    }).then(response => {
      var res = response.json();
      res.then((result) => {
        this.setState({
          points: result
        });
      });
    });
  }

  savePoints() {
    fetch('/rest/points/addPoints', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-Language': 'en-US,en;q=0.9',
        'Content-Type': 'application/json',
        mode: 'no-cors'
      },
      body:
        JSON.stringify(this.state.points)
    });
  }

  handleChangeFile(event) {
    const file = event.target.files[0];
    let formData = new FormData();
    formData.append('file', file);
    fetch('/rest/points/readPointsFromFile', {
      method:'POST',
      body: formData
    }).then(response => {
      var res = response.json();
      res.then((result) => {
        this.setState({
          points: result
        });
      });
    });
  }


  exportPointsToFile() {
    fetch('/rest/points/savePointsToFile', {
      method:'GET',
      headers: {
        Accept: 'application/json',
        'Accept-Language': 'en-US,en;q=0.9',
        'Content-Type': 'application/json',
        mode: 'no-cors'
      }
    }).then(response => {
      var res = response.blob();
      res.then((result) => {
        FileSaver.saveAs(result, 'exported_points.txt');
      });
    });
  }

  calculateSquares() {
    fetch('/rest/points/getSquares', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Accept-Language': 'en-US,en;q=0.9',
        'Content-Type': 'application/json',
        mode: 'no-cors'
      }
    }).then(response => {
      var res = response.json();
      res.then((result) => {
        this._inputElementNumbOfSquares.value = result.length;
        var combinations = new String("");
        for (let r of result) {
          var coordinate = new String("");
          for (let c of r) {
            coordinate = coordinate.concat("(");
            coordinate = coordinate.concat(c.x);
            coordinate = coordinate.concat(";");
            coordinate = coordinate.concat(c.y);
            coordinate = coordinate.concat(") ");
          }
          combinations = combinations.concat(coordinate);
          combinations = combinations.concat("\n");
        }
        this._inputElementCombOfSquares.value = combinations;
      });
    });
  }

  intVal(n: number | string): number {
    return typeof n === "number" ? n : parseInt(n, 10);
  }

  render() {
    return (
      <div className="pointListMain">
        <div className="header">
          <button id="retrieveFromServer" onClick={() => this.retrieveFromServer()}>Retrieve from server</button>
          <input type="file" onChange={this.handleChangeFile.bind(this)}/>
        </div>
        <div className="form">
          <form onSubmit={this.addPoint}>
            <input ref={(x) => this._inputElementX = x} placeholder="X coordinate" />
            <input ref={(y) => this._inputElementY = y} placeholder="Y coordinate" />
            <button type="submit">Add point</button>
          </form>
        </div>
        <PointItems points={this.state.points} delete={this.deletePoint}/>
        <div className="bottomButtons">
          <button id="clearAllButton" onClick={() => this.deleteAll()}>Clear all</button>
          <button id="saveButton" onClick={() => this.savePoints()}>Save to server</button>
          <button id="exportButton" onClick={() => this.exportPointsToFile()}>Export from server to file</button>
          <button id="calculateButton" onClick={() => this.calculateSquares()}>Calculate squares</button>
        </div>
        <div className="squares">
          <input ref={(x) => this._inputElementNumbOfSquares = x} placeholder="Number of squares" disabled="true"/>
          <br/>
          <textarea ref={(x) => this._inputElementCombOfSquares = x} placeholder="Combinations of squares" disabled="true"></textarea>
        </div>
        <div className="modal">
          <Modal
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}>
            <h2 ref={subtitle => this.subtitle = subtitle}>{this.state.errorMessage}</h2>
            <button id="closeModalButton" onClick={this.closeModal}>close</button>
          </Modal>
        </div>
      </div>
    );
  }

}

export default PointList;
