package lt.devbr.squares.controller;

import lt.devbr.squares.model.Point;
import lt.devbr.squares.service.PointService;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
@Path("/points")
public class PointController {

    private PointService pointService;

    public PointController(PointService pointService) {
        this.pointService = pointService;
    }

    @GET
    @Path("/getAllPoints")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Point> getAllPoints() {
        return pointService.getAllPoints();
    }

    @POST
    @Path("/addPoints")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPoints(List<Point> points) {
        pointService.addPoints(points);
        return Response.ok().build();
    }

    @POST
    @Path("/readPointsFromFile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public List<Point> getPointsFromFile(InputStream uploadedInputStream) {
        AtomicInteger delta = new AtomicInteger(0);
        List<Point> points = new BufferedReader(new InputStreamReader(uploadedInputStream))
                .lines().filter(s -> s.matches("\\d* \\d*"))
                .map(x -> new Point(Integer.parseInt(x.split(" ")[0]),
                        Integer.parseInt(x.split(" ")[1]), System.currentTimeMillis() + delta.getAndIncrement()))
                .collect(Collectors.toList());
        return points;
    }

    @GET
    @Path("/savePointsToFile")
    public Response downloadPointsAsFile(String fileName) {
        File file = pointService.pointsToFile();
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"")
                .build();
    }

    @GET
    @Path("/getSquares")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumberOfSquares() {
        return Response.ok(
                pointService.getNumberOfSquares()
        ).build();
    }

}