package lt.devbr.squares.service;

import lt.devbr.squares.model.Point;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PointService {

    private final List<Point> points;
    private final List<List<Point>> squares;

    public PointService() {
        points = new ArrayList<>();
        squares = new ArrayList<>();
    }

    public List<Point> getAllPoints() {
        return points.isEmpty() ? Collections.emptyList() : points;
    }

    public void addPoints(List<Point> points) {
        this.points.clear();
        this.points.addAll(points);
    }

    public File pointsToFile() {

        String pointsAsString = points.stream()
                .map(this::pointAsString)
                .collect(Collectors.joining());

        File file = new File("output.txt");
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            byte[] strToBytes = pointsAsString.getBytes();
            outputStream.write(strToBytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    public List<List<Point>> getNumberOfSquares() {
        return calculateSquares();
    }

    protected String pointAsString(Point point) {
        return point.getX() + " " + point.getY() + "\n";
    }

    protected boolean isSquare(List<Point> points) {
        double distance1 = getDistance(points.get(0), points.get(1));
        double distance2 = getDistance(points.get(1), points.get(2));
        double distance3 = getDistance(points.get(2), points.get(3));
        double distance4 = getDistance(points.get(3), points.get(0));

        if (distance1 == distance2 && distance2 == distance3 && distance3 == distance4 && distance4 == distance1) {
            return true;
        } else {
            distance1 = getDistance(points.get(0), points.get(2));
            distance2 = getDistance(points.get(2), points.get(1));
            distance3 = getDistance(points.get(1), points.get(3));
            distance4 = getDistance(points.get(3), points.get(0));
            if (distance1 == distance2 && distance2 == distance3 && distance3 == distance4 && distance4 == distance1) {
                return true;
            } else {
                distance1 = getDistance(points.get(0), points.get(2));
                distance2 = getDistance(points.get(2), points.get(3));
                distance3 = getDistance(points.get(3), points.get(1));
                distance4 = getDistance(points.get(1), points.get(0));
                if (distance1 == distance2 && distance2 == distance3 && distance3 == distance4 && distance4 == distance1) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    protected double getDistance(Point point1, Point point2) {
        return Math.hypot(point1.getX() - point2.getX(), point1.getY() - point2.getY());
    }

    protected List<List<Point>> calculateSquares() {
        squares.clear();
        recursiveCombinations(new ArrayList<>(), 0, points);
        return squares;
    }

    protected void recursiveCombinations(List<Point> combination, int n, List<Point> elements) {
        if (n == elements.size()) {
            if (combination.size() == 4 && isSquare(combination)) {
                squares.add(new ArrayList<>(combination));
            }
        } else {
            combination.add(elements.get(n));
            recursiveCombinations(combination, n + 1, elements);

            combination.remove(elements.get(n));
            recursiveCombinations(combination, n + 1, elements);
        }
    }

}