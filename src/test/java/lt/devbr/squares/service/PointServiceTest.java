package lt.devbr.squares.service;

import lt.devbr.squares.model.Point;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PointServiceTest {

    private PointService pointService = new PointService();

    @Test
    public void pointAsStringTest() {
        Point point = new Point(1, 3, 111);

        String pointAsString = pointService.pointAsString(point);

        assertEquals("1 3\n", pointAsString);
    }

    @Test
    public void isSquareTestFalse() {
        List<Point> points = new ArrayList<>();
        points.add(new Point(1, 1, 111));
        points.add(new Point(1, -1, 222));
        points.add(new Point(3, 1, 333));
        points.add(new Point(3, 5, 444));

        assertFalse(pointService.isSquare(points));
    }

    @Test
    public void isSquareTestFrue() {
        List<Point> points = new ArrayList<>();
        points.add(new Point(1, 1, 111));
        points.add(new Point(1, -1, 222));
        points.add(new Point(3, 1, 333));
        points.add(new Point(3, -1, 444));

        assertTrue(pointService.isSquare(points));
    }

}